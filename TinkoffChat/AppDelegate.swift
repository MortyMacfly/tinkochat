//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by flosk on 22/09/2018.
//  Copyreght © 2018 tumenbaev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  var currentState: State = State.notRunning
  enum State { case notRunning, inactive, active, background, suspended }
  
  func setState(_ method: String, nextState: State) {
    print("Application moved from \(currentState) to \(nextState): \(method)")
    currentState = nextState
  }
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    setState(#function, nextState: State.inactive)
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    setState(#function, nextState: State.inactive)
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    setState(#function, nextState: State.background)
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    setState(#function, nextState: State.inactive)
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    setState(#function, nextState: State.active)
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    setState(#function, nextState: State.notRunning)
  }
}
