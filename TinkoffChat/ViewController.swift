//
//  ViewController.swift
//  TinkoffChat
//
//  Created by flosk on 22/09/2018.
//  Copyright © 2018 tumenbaev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  var currentState: State = State.disappeared
  enum State { case appearing, appeared, disappearing, disappeared }
  
  func setState(_ method: String, nextState: State) {
    print("View moved from \(currentState) to \(nextState): \(method)")
    currentState = nextState
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(_ animated: Bool) {
    setState(#function, nextState: State.appearing)
  }
  
  override func viewDidAppear(_ animated: Bool) {
     setState(#function, nextState: State.appeared)
  }
  
  override func viewWillLayoutSubviews() {
    print(#function)
  }
  
  override func viewDidLayoutSubviews() {
    print(#function)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    setState(#function, nextState: State.disappearing)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    setState(#function, nextState: State.disappeared)
  }
}

